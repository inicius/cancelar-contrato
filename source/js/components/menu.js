const $menu = $('.js-menu')
const $firstLevel = $('.js-menu > li > a')

$firstLevel.on('click', function (e){
    e.preventDefault()
    const $this = $(this)
    const $submenu = $this.parent().find('ul')

    $menu.find('.is-active').removeClass('is-active')
    $submenu.addClass('is-active')
})