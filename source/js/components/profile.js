const $trigger = $('.js-dropdown-trigger')

$trigger.on('click', function () {
    const $this = $(this)
    const data = $this.data('dropdown-target')
    const parent = $this.parent()
    const $el = parent.find(data)

    const active = $this.parent().parent().find('.is-active')

    if ($el.hasClass('is-active')) {
        $this.stop(true, true).toggleClass('is-active')
        $el.stop(true, true).toggleClass('is-active')
    } else {
        active.removeClass('is-active')
        $this.stop(true, true).addClass('is-active')
        $el.stop(true, true).addClass('is-active')
    }
})

$trigger.on('clickout', function () {
    const $this = $(this)
    const active = $this.parent().parent().find('.is-active')
    active.stop(true, true).removeClass('is-active')
})