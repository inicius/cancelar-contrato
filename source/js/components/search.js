var $form = $('.js-pesquisa')

$form.on('submit', function (e) {
    e.preventDefault()
    var $this = $(this)
    var contractNumber = $this.find('input').val()   
    var url = 'http://' + window.location.host + '/Contrato/Cancelar/' + contractNumber
    window.location.assign(url)
})