// { 
//    "codUsuario": "ADMIN", //HARDCODED
//    "ip": "10.11.9.20", 
//    "dataAgendamento": "21/12/2017 10:50", // DATEPICKER
//    "cod_ts_contrato": "9900026393",
//    "num_contrato": "866448000",
//    "data_cancelamento": "21/12/2017 10:50", // DATEPICKER
//    "cod_cancelamento": 99,
//    "ind_copiar": "F",
//    "cod_ts_dest": "",
//    "cod_operadora_canc": ""
//  }

$('body').data('url', window.location.href)
var bodyurl = $('body').data('url')
var urlsplit = bodyurl.split('/')
var numContrato = urlsplit[urlsplit.length - 1]


$('.datepicker').datepicker({
    format: 'dd/mm/yyyy',
    startDate: new Date()
})


$('.form__cancelamento__contrato .btn-secondary').on('click', function () {
    $(this).parent().find('.btn-secondary').removeClass('active')
    $(this).addClass('active')
})

$('.form__cancelamento__contrato').one('submit', function (e) {
    e.preventDefault()
    var url = "http://10.11.10.86:5000/contrato"
    var dataval = $('.datepicker').val()
    dataval += ' 00:00'
    var indcopiar = $(this).find('.btn-secondary.active').data('type-cancel')
    var motivo = $('.novo-contrato-option option:selected').val()
    var obj = {
        "codUsuario": "ADMIN",
        "ip": "10.11.9.20",
        "dataAgendamento": dataval, // DATEPICKER
        "cod_ts_contrato": "9900026393",
        "num_contrato": numContrato,
        "data_cancelamento": dataval, // DATEPICKER
        "cod_cancelamento": Number(motivo),
        "ind_copiar": indcopiar,
        "cod_ts_dest": "",
        "cod_operadora_canc": ""
    }
    obj = JSON.stringify(obj)    

    return $.ajax({
        url: url,
        type: 'post',
        data: obj,
        headers: {
            "Content-Type": "application/json",
            "Accept" : "application/json"
        },
        dataType: "json",
        success: function (data) {
            if (data) {
                $('#cancel_queue').collapse()
                $('.cancelar__contrato .btn').fadeOut(400)
            } else {
                console.log('fail')
            }
        }
    })

    
        
})  