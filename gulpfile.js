// Requiring plugins
const gulp = require('gulp');
const babel = require('gulp-babel');
const sass = require('gulp-sass');
const uglify = require('gulp-uglify');
const cssnano = require('gulp-cssnano');
const rename = require('gulp-rename');
const imagemin = require('gulp-imagemin');
const cache = require('gulp-cache');
const del = require('del');
const runSequence = require('run-sequence');
const sourceMaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const pump = require('pump');

// Defining files source
const sassSrc = [
    './node_modules/bootstrap/dist/css/bootstrap.css',
    './node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.css',
    './source/css/**/*.scss'
];
const jsSrc = [
    './node_modules/jquery/dist/jquery.js',
    './node_modules/bootstrap/dist/js/bootstrap.bundle.js',
    './node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
    './source/js/**/*.js'    
];
const htmlSrc = '*.html';
const imageSrc = 'source/images/**/*.+(png|jpg|jpeg|gif|svg)';
const destCSS = './css';
const destJS = './js';

// Sass task
gulp.task('sass', function() {
    return gulp
        .src(sassSrc) // Defining sass source that will execute the first taks
        .pipe(concat('style.scss'))
        .pipe(sourceMaps.init()) // Initing sourcemaps plugin
        .pipe(sass()) // Executing Sass task to compile scss into css files
        .pipe(sourceMaps.write()) // Writing sourcemaps into CSS files
        .pipe(autoprefixer()) // Applying Autoprefixer
        .pipe(gulp.dest(destCSS)) // Sending single css file to app/css folder
        .pipe(cssnano()) // Minifying css
        .pipe(rename({
            suffix: '.min' // renaming to .min file
        }))
        .pipe(gulp.dest('./css')) // Transfering minified file to distribution
});

gulp.task('js', function(cb) {
    pump([
        gulp.src(jsSrc), // Defining js source that will execute the first taks
        babel(
            { presets: ['es2015', 'env'] }
        ),
        concat('bundle.min.js'),
        uglify(),
        gulp.dest(destJS)
    ], cb)
});

gulp.task('images', function(){
    return gulp
    .src(imageSrc) // Defining Images source path
    .pipe(cache(imagemin({ // Minifying images and creating cache
        interlaced: true
    })))
    .pipe(gulp.dest('./images')) // Transferring minified images to distribution
});


gulp.task('fonts', function() {
    return gulp
    .src('source/fonts/**/*') // Defining fonts source path
    .pipe(gulp.dest('./fonts')) // Transferring fonts to distribution
});

gulp.task('clean:dist', function() {
    return del.sync('./'); // Cleaning distribution folder
});

gulp.task('watch', ['sass', 'js'], function() {
    gulp.watch(sassSrc, ['sass']);
    gulp.watch(jsSrc, ['js']);
});

gulp.task('build', function (callback) {
    runSequence('clean:dist', 
        ['sass', 'js', 'images', 'fonts'],
        callback
    )
});

gulp.task('default', function (callback) {
    runSequence(['sass', 'js', 'images', 'watch'],
    callback
    )
});